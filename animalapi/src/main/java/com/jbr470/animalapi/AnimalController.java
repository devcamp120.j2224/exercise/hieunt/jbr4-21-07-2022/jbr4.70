package com.jbr470.animalapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class AnimalController {
    @GetMapping("/cats")
    public ArrayList<Cat> getCatList() {
        ArrayList<Cat> catList = new ArrayList<Cat>();
        Cat cat1 = new Cat("Nikki");
        Cat cat2 = new Cat("Mimi");
        Cat cat3 = new Cat("Loli");
        catList.add(cat1);
        catList.add(cat2);
        catList.add(cat3);
        return catList;
    }
    @GetMapping("/dogs")
    public ArrayList<Dog> getDogList() {
        ArrayList<Dog> dogList = new ArrayList<Dog>();
        Dog dog1 = new Dog("Milu");
        Dog dog2 = new Dog("Husky");
        Dog dog3 = new Dog("Lulu");
        dogList.add(dog1);
        dogList.add(dog2);
        dogList.add(dog3);
        return dogList;
    }
}
